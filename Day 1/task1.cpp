#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

int main()
{
    std::ifstream inputFile{"input.txt"};
    std::string line;
    std::uint32_t currentResult{0}, bestResult{0};
    char *end;

    while (getline(inputFile, line))
    {
        if (not line.length() == 0)
        {
            currentResult += std::strtoul(line.c_str(), &end, 10);
        }
        else
        {
            if ( currentResult > bestResult)
            {
                bestResult = currentResult;
            }
            currentResult = 0;
        }
    }

    if (inputFile.eof())
    {
        if (currentResult > bestResult)
        {
            bestResult = currentResult;
        }
    }

    inputFile.close();
    std::cout << "Best score: " << bestResult << std::endl;

    return 0;
}