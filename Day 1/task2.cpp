#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <numeric>

void sortBestResultsDecreasingly(std::vector<std::uint32_t> &bestResults)
{
    std::sort(bestResults.begin(), bestResults.end(), [](uint32_t& lhs, uint32_t& rhs){ return lhs > rhs; });
}

int main()
{
    std::ifstream inputFile{"input.txt"};
    std::string line;
    std::uint32_t currentResult{0};
    std::vector<std::uint32_t> bestResults;
    char *end;

    while (getline(inputFile, line))
    {
        if (not line.length() == 0)
        {
            currentResult += std::strtoul(line.c_str(), &end, 10);
        }
        else
        {
            if ( bestResults.size() < 3)
            {
                bestResults.push_back(currentResult);
                sortBestResultsDecreasingly(bestResults);
            }
            else if (currentResult > bestResults.back())
            {
                bestResults.pop_back();
                bestResults.push_back(currentResult);
                sortBestResultsDecreasingly(bestResults);
            }
            currentResult = 0;
        }
    }

    if (inputFile.eof())
    {
        if (currentResult > bestResults.back())
        {
            bestResults.pop_back();
            bestResults.push_back(currentResult);
        }
    }

    const auto sumOfBestResults = std::accumulate(bestResults.begin(), bestResults.end(), 0);

    inputFile.close();
    std::cout << "Best score: " << sumOfBestResults << std::endl;

    return 0;
}