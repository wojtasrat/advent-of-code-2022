#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

uint8_t calculateScore(std::string &oponentChoice, std::string &myChoice)
{
    uint8_t score = 0, draw = 3, win = 6, rock = 1, paper = 2, scissors = 3;
    if (myChoice == "X")
    {
        score += rock;
        if (oponentChoice == "A")
        {
            score += draw;
        }
        else if (oponentChoice == "C")
        {
            score += win;
        }
    }
    else if (myChoice == "Y")
    {
        score += paper;
        if (oponentChoice == "A")
        {
            score += win;
        }
        else if (oponentChoice == "B")
        {
            score += draw;
        }
    }
    else if (myChoice == "Z")
    {
        score += scissors;
        if (oponentChoice == "B")
        {
            score += win;
        }
        else if (oponentChoice == "C")
        {
            score += draw;
        }
    }

    return score;
}

int main()
{
    std::ifstream inputFile{"input.txt"};
    std::string oponentChoice, myChoice;
    std::uint32_t totalScore{0};

    while (not inputFile.eof())
    {
        inputFile >> oponentChoice;
        inputFile >> myChoice;
        totalScore += calculateScore(oponentChoice, myChoice);
    }

    inputFile.close();
    std::cout << "Total score: " << totalScore << std::endl;

    return 0;
}
