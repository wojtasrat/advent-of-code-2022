#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

uint8_t calculateScore(std::string &oponentChoice, std::string &myChoice)
{
    uint8_t score = 0, draw = 3, win = 6, rock = 1, paper = 2, scissors = 3;
    if (myChoice == "X")
    {
        if (oponentChoice == "A")
        {
            score += scissors;
        }
        else if (oponentChoice == "B")
        {
            score += rock;
        }
        else if (oponentChoice == "C")
        {
            score += paper;
        }
    }
    else if (myChoice == "Y")
    {
        score += draw;
        if (oponentChoice == "A")
        {
            score += rock;
        }
        else if (oponentChoice == "B")
        {
            score += paper;
        }
        else if (oponentChoice == "C")
        {
            score += scissors;
        }
    }
    else if (myChoice == "Z")
    {
        score += win;
        if (oponentChoice == "A")
        {
            score += paper;
        }
        else if (oponentChoice == "B")
        {
            score += scissors;
        }
        else if (oponentChoice == "C")
        {
            score += rock;
        }
    }

    return score;
}

int main()
{
    std::ifstream inputFile{"input.txt"};
    std::string oponentChoice, myChoice;
    std::uint32_t totalScore{0};

    while (not inputFile.eof())
    {
        inputFile >> oponentChoice;
        inputFile >> myChoice;
        totalScore += calculateScore(oponentChoice, myChoice);
    }

    inputFile.close();
    std::cout << "Total score: " << totalScore << std::endl;

    return 0;
}
